import React from 'react'
import { Dimensions, View, Text } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import NormalButton from '../components/Button/NormalButton/button'
import AuthHeader from '../components/header/auth'
import colors from '../themes/colors'


function Home(props) {

    return (
        <View style={{
            flex: 1,
            backgroundColor: colors.background
        }}>
            <View style={{
                flex: 2
            }}>
                <AuthHeader
                    header={"jdjjdhjdjdjd"}
                />
            </View>
            <View
                style={{
                    flex: 1,
                    backgroundColor: "white",
                    flexDirection: 'column',
                    borderTopRightRadius: 20,
                    borderTopLeftRadius: 20,
                }}
            >
                <View style={{
                    marginLeft: 20,
                    marginRight: 20
                }}>
                    <NormalButton
                        title="Login"
                        containerStyle={{
                            marginTop: 10,

                        }}
                        buttonStyle={{
                            borderRadius: 5,
                            height: 40,
                        }}
                        onPress={() => {
                            props.navigation.navigate("SignIn")
                        }}
                    />
                    <View style={{
                        flexDirection: 'row',
                        //   alignItems:'center',
                        justifyContent: 'center'
                    }}>
                        <View
                            style={{
                                width: Dimensions.get('screen').width / 2 - 40,
                                height: 2,
                                backgroundColor: "red",
                                marginTop: 20,
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}
                        />
                        <Text style={{
                            alignSelf: 'baseline'
                        }}>
                            or
                  </Text>
                        <View
                            style={{
                                width: Dimensions.get('screen').width / 2 - 40,
                                height: 2,
                                backgroundColor: "red",
                                marginTop: 20,
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}
                        />
                    </View>

                    <NormalButton
                        title="Create"
                        containerStyle={{
                            marginTop: 20
                        }}
                        buttonStyle={{
                            borderRadius: 5,
                            height: 40,
                        }}
                        onPress={() => props.navigation.navigate("SignUp")}
                    />
                </View>

            </View>
        </View>
    )
}

export default Home
