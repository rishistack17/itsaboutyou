import AuthHeader from '../../../components/header/auth';
import React,{useState,useEffect} from 'react'
import Serv from '../../../service/service'
import { inject, observer } from 'mobx-react'

import {
    View,
    SafeAreaView,
    Keyboard
} from "react-native";
import colors from '../../../themes/colors';
import NormalInput from '../../../components/Input/NormalInput/normal';
import NormalButton from '../../../components/Button/NormalButton/button';
import Floating from '../../../components/Button/FloatingButton/floating';


function SignIn(props) {
    const [keyboardStatus, setKeyboardStatus] = useState(false)
    const _keyboardDidShow=()=>setKeyboardStatus(true)
    const _keyboardDidHide=()=>setKeyboardStatus(false)
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    useEffect(() => {
      Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
      Keyboard.addListener("keyboardDidHide", _keyboardDidHide);
  
      // cleanup function
      return () => {
        Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
        Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
      };
    }, []);
    return (
        <SafeAreaView style={{
            flex: 1,
            backgroundColor: colors.background
        }}>
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <AuthHeader header={"Welcome Back"} />
            </View>
            <View style={{
                flex: 2,
                backgroundColor: colors.textcolor,
                borderTopRightRadius: 20,
                borderTopLeftRadius: 20,
                flexDirection: 'column'
            }}>
                <View style={{
                    flex: 1,
                    marginRight: 20,
                    marginLeft: 20,
                }}>
                    <View style={{
                        alignItems: 'center'
                    }}>
                        <AuthHeader header={"Sign In"} authText={{
                            color: colors.signupHeader
                        }} />
                    </View>
                    <View style={{
                        flex: 1
                    }}>
                        <NormalInput
                            label="Email"
                            title="Email@gmail.com"
                            placeholderColor={colors.placeholdercolor}
                            onChangeText={(text)=>setEmail(text)}

                        />
                    </View>
                    <View style={{
                        flex: 1
                    }}>
                        <NormalInput
                            label="Password"
                            title="............"
                            type={"visible-password"}
                            placeholderColor={colors.placeholdercolor}
                            onChangeText={(text)=>setPassword(text)}

                        />
                    </View>
                    {keyboardStatus?
                    null
                    :
                    <View style={{
                        flex: 1
                    }}>
                        <NormalButton
                        title="Sign In"
                        buttonStyle={{
                            borderRadius:5,
                            height:40,
                        }}
                        onPress={() => 
                            {
                                let data={
                                    
                                    email:email,
                                    password:password
                                }
                                Serv.loginUser(data)
                                .then(res=>{
                                    console.log('====================================');
                                    console.log(res);
                                    console.log('====================================');
                                    props.store.setEmail(email)
                                    props.store.setID(res.data.id)
                                    props.store.setToken(res.data.token)
                                    props.navigation.navigate("Landing")

                                }
                                )
                                .catch(e=>console.log(e))
                            }
                        }
                        />
                        <Floating
                            title={"Sign up?"}
                            color={colors.button}
                            containerStyle={{
                                marginTop:20
                            }}
                            onPress={()=>props.navigation.navigate("SignUp")}
                        />
                    </View>
                    }
                    
                </View>

            </View>
        </SafeAreaView>
    )
}

export default inject("store")(observer(SignIn))
