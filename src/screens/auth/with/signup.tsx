import React, { useState, useEffect } from 'react'
import { SafeAreaView,View, Text, Keyboard } from 'react-native'
import AuthHeader from '../../../components/header/auth'
import colors from '../../../themes/colors'
import {NormalButton,NormalInput,FloatingButton} from "../../../components/index"
import Serv from '../../../service/service'
import { inject, observer } from 'mobx-react'

import us from '../../../state/store'
//TODO:user store doesnt has actual token
function SignUp(props) {
    const [keyboardStatus, setKeyboardStatus] = useState(false)
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [name,setName] = useState("")
    const _keyboardDidShow = () => setKeyboardStatus(true)
    const _keyboardDidHide = () => setKeyboardStatus(false)
    useEffect(() => {
        Keyboard.addListener("keyboardDidShow", _keyboardDidShow);
        Keyboard.addListener("keyboardDidHide", _keyboardDidHide);

        // cleanup function
        return () => {
            Keyboard.removeListener("keyboardDidShow", _keyboardDidShow);
            Keyboard.removeListener("keyboardDidHide", _keyboardDidHide);
        };
    }, []);
    return (
        <SafeAreaView style={{
            flex: 1,
            flexDirection: 'column',
            backgroundColor: colors.background
        }}>
            <View style={{
                flex: 1,
                backgroundColor: "transparent",
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <AuthHeader
                    header={"Create a new account"}
                />
            </View>
            <View style={{
                flex: 6,
                backgroundColor: 'white',
                borderTopRightRadius: 20,
                borderTopLeftRadius: 20
            }}>
                <View style={{
                    flex: 1,
                    marginRight: 20,
                    marginLeft: 20,
                }}>
                    <View style={{
                        alignItems: 'center'
                    }}>
                        <AuthHeader header={"Sign Up"} authText={{
                            color: colors.signupHeader
                        }} />
                    </View>
                    <View style={{
                        flex: 1
                    }}>
                        <NormalInput
                            label="Select a username"
                            title="name"
                            placeholderColor={colors.placeholdercolor}
                            onChangeText={(text)=>setName(text)
                            }
                        />
                    </View>
                    <View style={{
                        flex: 1
                    }}>
                        <NormalInput
                            label="Email"
                            title="Email@gmail.com"
                            placeholderColor={colors.placeholdercolor}
                            onChangeText={(text)=>setEmail(text)}

                        />
                    </View>
                    <View style={{
                        flex: 1
                    }}>
                        <NormalInput
                            label="Password"
                            title="............"
                            type={"visible-password"}
                            placeholderColor={colors.placeholdercolor}
                            onChangeText={(text)=>{setPassword(text);console.warn(text);
                            }}
                        />
                    </View>
                    {keyboardStatus ?
                        null
                        :
                        <View style={{
                            flex: 1
                        }}>
                            <NormalButton
                                title="Sign up"
                                buttonStyle={{
                                    borderRadius: 5,
                                    height: 40,
                                }}
                                onPress={() => 
                                    { 
                                        let data={
                                            name:name,
                                            email:email.trim(),
                                            password:password
                                        }
                                        Serv.registerUser(data)
                                        .then(res=>{
                                            props.store.setToken(res.type)
                                            props.store.setEmail(res.data.email)
                                            props.navigation.navigate("Landing")

                                        }
                                        )
                                        .catch(e=>console.log(e))
                                    }
                                }
                            />
                            <FloatingButton
                                title={"Login?"}
                                color={colors.button}
                                containerStyle={{
                                    marginTop: 20
                                }}
                                onPress={()=>props.navigation.navigate("SignIn")}

                            />
                        </View>
                    }
                    <View style={{
                        flex: 0.5
                    }}>
                        
                    </View>
                </View>
            </View>
        </SafeAreaView>
    )
}

export default inject("store")(observer(SignUp))
