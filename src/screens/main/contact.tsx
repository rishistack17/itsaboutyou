import { SERVER_ERROR } from 'apisauce'
import React ,{useState}from 'react'
import { Text, View, FlatList } from 'react-native'
import { AddContactModal, AddTaskModal, BaseModal, Float, Header } from '../../components'
import ContactCard from '../../components/card/container/contact'
import Serv from '../../service/service'
import {inject,observer} from "mobx-react"
import { showToastWithGravity } from '../../utils/toast'
const contacts = [{
    id: 1,
    name: "rishi",
    email: "rishi"
},
{
    id: 2,
    name: "rishi",
    email: "rishi"
},
{
    id: 3,
    name: "rishi",
    email: "rishi"
},
{
    id: 4,
    name: "rishi",
    email: "rishi"
}, {
    id: 5,
    name: "rishi",
    email: "rishi"
}, {
    id: 6,
    name: "rishi",
    email: "rishi"
}, {
    id: 7,
    name: "rishi",
    email: "rishi"
}, {
    id: 8,
    name: "rishi",
    email: "rishi"
}, {
    id: 9,
    name: "rishi",
    email: "rishi"
}, {
    id: 10,
    name: "rishi",
    email: "rishi"
}
]
//TODO:look how i didi for homw page replicate it 
const render = ({ item }) => {
    return (
        <View style={{
            marginTop: 10,
            alignItems: 'center'
        }}>
            <ContactCard name={item.name}
                email={item.email} />
        </View>
    )
}
function Contact(props) {
    const [addContact, setAddContact] = useState(false)
    const [email, setemail] = useState("")
    const [phone, setphone] = useState("")
    const [name, setname] = useState("")
    return (
        <View
            style={{
                flex: 1
            }}
        >
            <AddContactModal
            visible={addContact}
            transparent={true}
            handleRejection={()=>{
                setAddContact(false)
            }}
            onChangeEmail={(text)=>setemail(text)
            }
            onChangeName={text=>setname(text)}
            onChangePhone={text=>setphone(text)}
            handleSubmission={
                ()=>{
                    let data={
                        "user":props.store.email,
                        "contact":{
                            "name":name,
                            "email":email,
                            "phone":phone
                        }
                    }
                    Serv.addContact(data,props.store.token)
                    .then(res=>{
                        console.log('====================================');
                        console.log(res);
                        console.log('====================================');
                        setAddContact(false);
                        showToastWithGravity("contact added successfully!")
                    })
                    .catch(e=>{
                        console.log('====================================');
                        console.log(e);
                        console.log('====================================');
                    })
                    
                }
            }
            />
            <View style={{
                flex: 1,opacity:addContact?0.4:1
            }}>
                <Header />
            </View>
            <View style={{
                flex: 15,opacity:addContact?0.4:1
            }}>
                <FlatList
                    data={contacts}
                    renderItem={render}
                />
            </View>
            <View
                style={{
                    flex: 0.3,
                    backgroundColor: 'transparent'
                }}
            />
            <View
                style={{
                    flex: 1,
                    position: "absolute",
                    alignSelf: 'flex-end',
                    bottom: 20,
                    right: 10,
                    alignItems: 'flex-start',
                    opacity: addContact ? 0.4 : 1
                }}
            >
                <Float
                    name={"plus"}
                    onPress={() => setAddContact(true)}
                />

            </View>
        </View>
    )
}

export default inject("store")(observer(Contact));
