import React, { useEffect, useState } from 'react'
import { Text, View, FlatList } from 'react-native'
import { inject, observer } from 'mobx-react'
import { AddTaskModal, BaseModal, Float, Header } from '../../components'
import TaskCard from '../../components/card/container/task'
// import Header from "../../components/header/header"
import Serv from '../../service/service'
import { showToastWithGravity } from '../../utils/toast'
import { toJS } from 'mobx'
import todo from '../../state/task'
import Todo from '../../state/task'
// import { setTasks } from '../../hooks/task'


//TODO:problem is that bottom tab bar cover most of the are

const tasks = [
    {
        id: 1,
        sub: "rishijha",
        des: "hshhshsssllsl"
    },
    {
        id: 2,
        sub: "hhddhhoaH",
        des: "hhgygedlsrufhiokxxvijxd",

    }
    , {
        id: 3,
        sub: "agidyrgwlgfsnksf",
        des: "higsdrtgigughusg"
    }, {
        id: 4,
        sub: "htu5ehrugh",
        des: "jshuiru4yu"
    }, {
        id: 5,
        sub: "htu5ehrugh",
        des: "jshuiru4yu"
    }, {
        id: 6,
        sub: "htu5ehrugh",
        des: "jshuiru4yu"
    }
]
const render = ({ item }) => {
    return (
        <View style={{
            marginTop: 10,
            alignItems: 'center'
        }}>
            <TaskCard
                subject={item.sub}
                description={item.des}
                dueDate={98998}
            />
        </View>
    )
}

function HomePage(props) {
    const [addTask, setaddTask] = useState(false)
    const [subject, setsubject] = useState("")
    const [des, setdes] = useState("")
    const [dateTime, setdateTime] = useState("")
    const [task, setTasks] = useState([])
    const [id,setId] =useState('')
    useEffect(( ) => {
        // setId(props.store.id)
        // Serv.getAllTasks(props.store.id,props.store.token)
        // .then(res=>console.log(res?.data))  
        // .catch(e=>console.log(e))
    }, [])

    return (
        <View style={{
            flex: 1
        }}>
            <AddTaskModal
                visible={addTask}
                transparent={true}
                onChangeSubject={(text) => setsubject(text)}
                onChangeDescription={(text) => setdes(text)}
                onChangeDateTime={(text) => setdateTime(text)}
                handleRejection={() => setaddTask(false)}
                handleSubmission={() => {
                    console.log('====================================');
                    console.log(props.store.email);
                    console.log('====================================');
                    let todo = {
                        "task": {
                            "subject": subject,
                            "description": des,
                            "duedate": dateTime
                        },
                        "user": props.store.email

                    }
                    console.log('====================================');
                    console.log(todo);
                    console.log('====================================');
                    Serv.addTask(todo, props.store.token,props.store.id)
                        .then(res => {
                            console.log(res)
                            setaddTask(false)
                            showToastWithGravity("task added sucssessfully!")
                        })
                        .catch(e => console.log(e))
                }}
            />

            <View style={{
                flex: 1,
                opacity: addTask ? 0.4 : 1
            }}>
                <Header />
            </View>
            <View style={{
                flex: 15,
                opacity: addTask ? 0.4 : 1

            }}>
                <FlatList
                    data={tasks}
                    renderItem={render}
                />
            </View>
            <View
                style={{
                    flex: 0.3,
                    backgroundColor: 'transparent'
                }}
            >
            </View>
            <View
                style={{
                    flex: 1,
                    position: "absolute",
                    alignSelf: 'flex-end',
                    bottom: 20,
                    right: 10,
                    alignItems: 'flex-start',
                    opacity: addTask ? 0.4 : 1
                }}
            >
                <Float
                    name={"plus"}
                    onPress={() => {
                        setaddTask(true)
                        let data={
                            'user':props.store.id
                        }
        //                 Serv.getAllTasks(data,props.store.token)
        // .then(res=>console.log(res?.data))
                        // Serv.getAllTasks(props.store.id, props.store.token)
                        //     .then(res => {
                        //         // setTasks(res?.data);
                        //         console.log('====================================');
                        //         console.log(res?.data);
                        //         console.log('====================================');
                        //         // setTasks(res?.data)
                        //         Todo.setTasks(res?.data)
                        //         console.log('====================================');
                        //         console.log(Todo.tasks);
                        //         console.log('====================================');
                        //     })
                        //     .catch(e => console.log(e))
                    }}
                />

            </View>

        </View>
    )
}

export default inject("store")(observer(HomePage))
