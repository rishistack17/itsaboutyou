import { Dimensions } from "react-native";

const dims ={
    width:Dimensions.get('screen').width,
    height:Dimensions.get('screen').height
    
}

export default dims;