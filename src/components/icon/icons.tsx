import React from 'react'
import { View } from 'react-native'
import AntD from "react-native-vector-icons/AntDesign"
import Micon from "react-native-vector-icons/MaterialCommunityIcons"
function Icon(props) {
    return (
        <View>
            <AntD
            name={props.name}
            size={props.size}
            color={props.color}
            />
        </View>
    )
}
function MaterialCIcons(props) {
    return (
        <View>
            <Micon
            name={props.name}
            size={props.size}
            color={props.color}
            />
        </View>
    )
}

export  {Icon,MaterialCIcons}
