import React from 'react'
import { View ,Text,TextInput} from 'react-native';
import colors from '../../../themes/colors';

const NormalInput = (props:any) =>{
    return (
        <View style={[{
            flexDirection:'column'
        },
            props.containerStyle
        ]}>
            {
                typeof props.label != typeof undefined ?
                    (
                        <View style={{
                            display:'flex',
                            flexDirection:'row',
                            marginBottom:30
                        }}>
                            <View style={{
                                flex:1
                            }}>
                                <Text style={
                                    [{
                                        fontWeight:'bold'
                                    },props.labelStyle]
                                }>
                                    {props.label}
                                </Text>
                            </View>
                            {props.extra}
                        </View>
                    )
                    :
                    null
            }

            
                <TextInput  
                    placeholder={props.title}
                    placeholderTextColor={props.placeholderColor}
                    
                    style={[
                        {
                            backgroundColor:colors.shadyBackground,
                            height:50,
                            borderRadius:5,
                        },
                        props.inputStyle
                    ]}
                    multiline={props.multiline}
                    keyboardType={props.type}
                    onChangeText={props.onChangeText}
                    
                />
            {/* </View> */}
            
        </View>
    )
}


export default NormalInput