import React, { Children } from 'react'
import { TouchableOpacity, View } from 'react-native'
import { FloatingButton } from '../../..'
import { Icon } from '../../../icon/icons'
const childeren =(props)=>{
    return (

            <Icon
            name={props.name}
            size={30}
            color={"white"}
            />

    )
}
function FloatIcon(props) {
    return (
        <View style={[{
            height:60,
            width:60,
            borderRadius:60,backgroundColor:"red",alignItems:'center',
            justifyContent:'center'
        },props.style]}>
            <TouchableOpacity
            onPress={props.onPress}
            >
                {childeren(props)}
            </TouchableOpacity>
        </View>
    )
}

export default FloatIcon
