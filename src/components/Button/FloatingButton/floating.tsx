import React from 'react'
import {TouchableOpacity, View,Text} from "react-native"
function Floating(props:any) {
    return (
        <View style={[{display:'flex'},props.containerStyle]}>
            <TouchableOpacity 
            style={[{},props.buttonStyle]}
            onPress ={props.onPress}
            >
                <Text style={{
                    color:props.color,
                    fontWeight:'500'
                }}>
                    {props.title}
                </Text>
                {props.childeren}
            </TouchableOpacity>
         </View>
    )
}
export default Floating
