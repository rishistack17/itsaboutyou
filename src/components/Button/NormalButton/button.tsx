import React from 'react'
import {
    TouchableOpacity,
    View,
    Text
} from "react-native";
import colors from '../../../themes/colors';

function NormalButton(props:any) {
    return (
        <View style={[
            {

            },
            props.containerStyle
        ]}>
            <TouchableOpacity
            onPress={props.onPress}
            style={[{
                backgroundColor:colors.button,
                height:40
            },props.buttonStyle]}
            >
                <Text 
                style={[
                    {
                        color:"white",
                        fontSize:20,
                        textAlign:'center'
                    },
                    props.titleColor

                ]}
                >
                    {props.title}
                </Text>
            </TouchableOpacity>
        </View>
    )
}

export default NormalButton
