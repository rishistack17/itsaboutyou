import React from 'react'
import { Modal, View } from 'react-native'

function BaseModal(props) {
    return (
        <View style={{
            // flex:1
            backgroundColor:'transparent'
        }}>
            <Modal
            visible={props.visible}
            transparent={props.transparent}
            >
                {props.children}
            </Modal>
         </View>
    )
}

export default BaseModal
