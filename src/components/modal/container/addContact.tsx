import React from 'react';
import { View } from 'react-native';
import { Float } from '../..';

import dims from '../../../contants/dims';
import NormalInput from '../../Input/NormalInput/normal';
import BaseModal from '../modal';
const childeren = (props) => {
    return (
        <View
            style={{
                flex: 1,
                flexDirection: 'column',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',

            }}
        >

            <View
                style={{
                    flex: 0.5,
                }}
            >
                <View style={{
                    width: dims.width - 70,
                    flex: 2
                }}>
                    <NormalInput
                        title={"subject"}
                        placeholderColor={"green"}
                        inputStyle={{

                        }}
                        containerStyle={{  
                            marginBottom:20
                        }}
                        onChangeText={props.onChangeName}
                    />

                    <NormalInput
                        title={"date-time"}
                        placeholderColor={"green"}
                        inputStyle={{
                            width:160
                        }}
                        containerStyle={{
                            marginBottom:20
                        }}
                        onChangeText={props.onChangeEmail}
                    />
                    <NormalInput
                        title={"date-time"}
                        placeholderColor={"green"}
                        inputStyle={{
                            width:160
                        }}
                        containerStyle={{
                            marginBottom:20
                        }}
                        onChangeText={props.onChangePhone}
                    />
                </View>
                <View
                style={{
                    flex:1,
                    flexDirection:'row'
                }}
                >
                    <View
                    style={{
                        flex:1,
                        alignItems:'center',
                        justifyContent:'center'
                    }}
                    >
                        <Float
                         name={"check"}
                         onPress={props.handleSubmission}

                        />

                    </View>
                    <View
                    style={{
                        flex:1,
                        alignItems:'center',
                        justifyContent:'center'
                    }}
                    >
                        <Float
                         name={"close"}
                         onPress={props.handleRejection}

                        />

                    </View>
                </View>


            </View>


        </View>
    )
}
function AddContact(props) {
    return (

        <BaseModal visible={props.visible} transparent={props.transparent} children={childeren(props)} />
    )
}

export default AddContact;
