import React from 'react'
import { Text, View } from 'react-native'
import dims from '../../../contants/dims'
import Card from '../card'
const childeren = (props) => {
    return (
        <View style={{
            flex: 1,
            marginLeft:20,
            marginRight:20
        }}>
            <View style={{
                flex: 1, justifyContent: 'center',
                
            }}>

                <View style={{
                    flex: 1,
                    flexDirection: 'row'
                }}>
                    <Text style={{

                        fontSize: 15
                    }}>
                        {props.subject}
                    </Text>
                    <View style={{
                        flex:1,
                        alignItems:'flex-end'
                    }}>
                        <Text>
                            {props.dueDate}
                        </Text>
                    </View>
                </View>
            </View>
            <View
                style={{
                    flex: 2, justifyContent: 'center'
                }}
            >
                <Text style={{

                    fontSize: 20
                }}>
                    {props.description}

                </Text>
            </View>
        </View>
    )
}
function TaskCard(props) {

    return (
        <Card style={{

            height:(dims.height-150 )/5,

        }} 
        childeren={childeren(props)} />
    )

}

export default TaskCard;
