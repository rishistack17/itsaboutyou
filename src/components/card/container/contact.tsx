import React from 'react'
import { Text, View } from 'react-native'
import Card from '../card'
const childeren = (props) => {
    return (
        <View style={{
            flex: 1,
            marginLeft:20,
             marginRight:20

        }}>
            <View style={{
                flex: 1,justifyContent:'center'
            }}>
                <Text style={{

                    fontSize:15
                }}>
                    {props.name}
                </Text>
            </View>
            <View
                style={{
                    flex: 2,justifyContent:'center'
                }}
            >
                <Text style={{

                    fontSize:20
                }}>
                    {props.email}

                </Text>
            </View>
        </View>
    )
}
function ContactCard(props) {
    return (
        <Card childeren={childeren(props)} />
    )
}

export default ContactCard
