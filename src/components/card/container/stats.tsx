import React from 'react'
import { Text, View } from 'react-native'
import dims from '../../../contants/dims'
import Card from '../card'
const childeren = (props) => {
    return (
        <View style={{
            flex: 1,
            marginLeft:20,
             marginRight:20

        }}>
            <View style={{
                flex: 3,justifyContent:'center'
            }}>
                <Text style={{

                    fontSize:15
                }}>
                    {props.name}
                </Text>
            </View>
            <View
                style={{
                    flex: 1,justifyContent:'center',backgroundColor:'green',flexDirection:'column'
                }}
            >
               <View 
                style={{
                   flex:1,flexDirection:'row'
               }}>
                   <View style={{
                       flex:1
                   }}>
                       <Text>
                           Total Earning : {props.earning}
                       </Text>
                   </View>
                   <View style={{
                       flex:1
                   }}>
                       <Text>
                           Total saving:{props.saving}
                       </Text>
                   </View>

               </View>
               <View 
                style={{
                   flex:1,flexDirection:'row'
               }}>
                   <View style={{
                       flex:1
                   }}>
                       <Text>
                       Total task: {props.totalTasks}

                       </Text>
                   </View>
                   <View style={{
                       flex:1
                   }}>
                       <Text>
                       complteted tasks: {props.tasksCompleted}
                       </Text>
                   </View>

               </View>
            </View>
        </View>
    )
}
function StatsCard(props) {

    return (
        <Card style={{

            height:(dims.height-150 )/2,

        }} 
        childeren={childeren(props)} />
    )

}

export default StatsCard;
