import React from 'react'
import { View } from 'react-native'
import dims from '../../contants/dims'

function Card(props) {
    return (
        <View style={[{
            flexDirection:'column',
            width:dims.width-20,
            height:(dims.height-150 )/10,
            // backgroundColor:'red',
            borderRadius:20,
        },props.style]}>
        
            {props.childeren}
        </View>
    )
}

export default Card
