import React from 'react'
import { TouchableOpacity, View } from 'react-native'
import dims from '../../contants/dims'
import { MaterialCIcons } from '../icon/icons'

function Header() {
    return (
        <View style={{
            height: 50, width: dims.width,
            display: 'flex',
            flexDirection: 'row',
            // backgroundColor: 'red'

        }}>
            <View style={{
                flex: 1,
                justifyContent: 'center'
            }}>
                <TouchableOpacity
                    onPress={() => console.warn('puup')}
                >
                    <View
                        style={{
                            height:35,
                            width:35,
                            borderRadius: 30,
                            backgroundColor: "green",
                            marginLeft: 20

                        }}
                    />
                </TouchableOpacity>
            </View>
            <View style={{
                flex: 1,
                flexDirection: 'row'
            }}>
               
                <View
                    style={{
                        flex: 1,
                        justifyContent:'center',
                        alignItems:'flex-end'

                    }}
                >
                    <MaterialCIcons
                        name={"bell-outline"}
                        size={25}
                        color={"black"}
                    />
                </View>
            </View>
        </View>
    )
}

export default Header
