import React from 'react'
import {View,Text} from "react-native";
import colors from '../../themes/colors';

function AuthHeader(props:any) {
    return (
        <View style={[
            {
                flexDirection:'column'
            }
        ]}>
            <Text style={[{
                   fontWeight:'bold' ,
                   fontSize:30,
                   color:colors.textcolor
            },
            props.authText
            ]}>
                {props.header}
            </Text>
        </View>
    )
}

export default AuthHeader
