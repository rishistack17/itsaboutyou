const colors ={
    background:"#7d4cd9",//use for signup,
    button:"#eb3437",//use fr button
    shadyBackground:"#615159",
    placeholdercolor:"#968080",
    textcolor:"#e0dadf",
    signupHeader:"#42395c"
}
export default colors;