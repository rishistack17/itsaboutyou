import { useEffect, useState } from "react"
import Serv from "../service/service"

const setTasks=(id:string,token:string)=>{
    const[tasks,setTasks]=useState([])
    useEffect(()=>{
            Serv.getAllTasks(id,token)
            .then(
                (res)=>setTasks(res?.data)
            )
            .catch(e=>console.log(e))
    },[])
    return tasks
}

export {
    setTasks
}