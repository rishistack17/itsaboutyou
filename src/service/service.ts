import { ApisauceInstance, create, ApiResponse } from 'apisauce'
import itsay from '../config/api'


class Service {
  apisauce: ApisauceInstance


  constructor() {

    this.apisauce = create({
      baseURL: itsay.url,
      // baseURL: 'http://34.96.120.243/v1',
      headers: {
        Accept: 'application/json'
      }
    })
  }

  // TODO: Wrap all these calls in try-catch to handle api errors. Display errors in the UI to let the user know

  async testone() {
    const response: ApiResponse<any> = await this.apisauce.get('/')
    if (response.ok) {
      return response.data.weather
    }
  }
  async registerUser(req:object){
    const response:ApiResponse<any>=await this.apisauce.post("/user/",req)
    if (response.ok){
      return response.data
    }

  }
  async loginUser(req:object){
    const response:ApiResponse<any>=await this.apisauce.post("/user/login",req)
    if (response.ok){
      return response.data
    }
  }
  async addTask(req:object,token:string,id:string){
    this.apisauce.setHeader("Authorization",token)//"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2MjI1NTQ4NzEsInBhc3N3b3JkIjoiJDJhJDEwJFpwWWd4cy9NUndhcWJMdUh3RWF5TGV0UVBwazR3Z0N4ZXQ4clk5dXlJcG9iaXRFTlZCWkVPIiwidXNlcl9pZCI6IjYwYWNmZDExMmJlYzU0NjViM2FhY2ZmZCJ9.rxaS0kE75WV2mAfDxD-RxE3HeCFjU4Jsyi8Iav3thsk")
    const response:ApiResponse<any>=await this.apisauce.post("/task/"+id,req)
    if (response.ok){
      return response.data
    }
  }
  async addContact(req:object,token:string){
    this.apisauce.setHeader("Authorization",token)//"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2MjI1NTQ4NzEsInBhc3N3b3JkIjoiJDJhJDEwJFpwWWd4cy9NUndhcWJMdUh3RWF5TGV0UVBwazR3Z0N4ZXQ4clk5dXlJcG9iaXRFTlZCWkVPIiwidXNlcl9pZCI6IjYwYWNmZDExMmJlYzU0NjViM2FhY2ZmZCJ9.rxaS0kE75WV2mAfDxD-RxE3HeCFjU4Jsyi8Iav3thsk")
    const response:ApiResponse<any>=await this.apisauce.post("/contact/",req)
    if (response.ok){
      return response.data
    }
  }
  //TODO:blunder bejar hai
  async getAllTasks(id:string,token:string)
  {
    this.apisauce.setHeader("Authorization",token)
    const response:ApiResponse<any>=await this.apisauce.get("/task/"+id)
    if (response.ok){
      return response
    }
  }
}

const Serv = new Service()

export default Serv
