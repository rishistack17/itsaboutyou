import { ToastAndroid } from "react-native"

const showToastWithGravity =(msg:string)=>{
    ToastAndroid.showWithGravity(
        msg,
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM
    )
}

export {
    showToastWithGravity
}