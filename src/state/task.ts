import { action, computed, observable } from "mobx";
class todo {
    @observable public tasks:any[];
    constructor()
    {
        this.tasks=[];
    }
    @action
    public setTasks(tasks :any[]){
        this.tasks=tasks
    }
    @computed
    public getTasks():any[]{
        return this.tasks
    }
}

const Todo=new todo()
export default  Todo