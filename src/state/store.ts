import { observable, action, computed, reaction } from "mobx";
class user{
    @observable public email:string;
    @observable public token:string;
    @observable public id:string;
    constructor()
    {
        this.email=""
        this.token=""
        this.id=""
        reaction(()=>this.email,_=>console.log(this.email))
    }
    @action setEmail=(nm:string)=>{
        this.email=nm
    }
    @action setToken=(token:string)=>{
        this.token=token
    };
    @action setID=(id:string)=>{
        this.id=id
    }
    @computed  
    public get info() : string {
        return this.email
    }

    @computed
    public get authToken():string{
        return this.token
    }

    @computed 
    public get ID():string{
        return this.id
    }
    
    
}
const us = new user();
export default us;