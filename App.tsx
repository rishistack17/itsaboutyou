/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { useEffect, useState } from 'react';
import NetInfo from "@react-native-community/netinfo"
import { createStackNavigator } from "@react-navigation/stack"
import { NavigationContainer } from "@react-navigation/native"
import Home from './src/screens/home';
import LoadingScreen from "./src/screens/loading"
import { SignUp, SignIn, CreateAccount, LoginAccount } from "./src/screens/auth/index"
import { Dimensions, SafeAreaView, Text, View, TouchableOpacity } from 'react-native';
import Land from './src/screens/main/add';
import { Provider } from 'mobx-react';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import us from './src/state/store';
import Contact from './src/screens/main/contact';
import HomePage from './src/screens/main/homepage';
import Stats from './src/screens/main/stats';
import Meetup from './src/screens/main/meetup';
import colors from './src/themes/colors';
import { Icon, MaterialCIcons } from './src/components/icon/icons';

const defaultNavigationSeetings = {
   headerShown: false
}
const defaultTabConfig = {

}
const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const TabsNavigation = () => {
   return (
      <Tabs.Navigator
         tabBarOptions={{
            showLabel: false,
            style: {
               backgroundColor: colors.background,
               width: Dimensions.get('screen').width - 20,
               alignSelf: 'center',
               borderRadius: 20,
               height: 55,
               bottom: 10
            }
         }}
      >
         <Tabs.Screen
            name="diary"
            component={Contact}
            options={{
               tabBarIcon: () => {
                  return (
                     <MaterialCIcons
                        name={"notebook"}
                        size={25}
                        color={"black"}
                     />

                  )
               }
            }}
         />
         <Tabs.Screen
            name="contacts"
            component={Contact}
            options={{
               tabBarIcon: () => {
                  return (
                     <Icon
                        name={"contacts"}
                        size={25}
                        color={"black"}
                     />
                  )
               }
            }}
         />
         <Tabs.Screen
            name="landing"
            component={HomePage}
            options={{

               tabBarButton: (props) => {
                  return (
                     <View style={{
                        width: 55,
                        height: 53,
                        backgroundColor: "white",
                        // bottom:25,
                        alignItems: 'center',
                        justifyContent: 'center',
                        borderRadius: 30
                     }}>
                        <TouchableOpacity
                           style={{
                              // backgroundColor: "red",
                              width: 50,
                              height: 50,
                              borderRadius: 50,
                              alignItems: 'center',
                              justifyContent: 'center',
                           }}
                           onPress={props.onPress}
                        >
                           <Icon
                              name={"home"}
                              size={35}
                              color={"black"}
                           />
                        </TouchableOpacity>
                     </View>

                  )
               }
            }}
         />
         <Tabs.Screen
            name="stats"
            component={Stats}
            options={{
               tabBarIcon: () => {
                  return (
                     <Icon
                        name={"barchart"}
                        size={25}
                        color={"white"}
                     />
                  )
               }
            }}
         />
         <Tabs.Screen
            name="meetup"
            component={Meetup}
            options={{
               tabBarIcon: () => {
                  return (
                     <Icon
                        name={"team"}
                        size={25}
                        color={"white"}
                     />
                  )
               }
            }}
         />

      </Tabs.Navigator>
   )
}
const App = () => {
   const [Net, setNet] = useState(false)
   const [authenticated, setauthenticated] = useState(false)
   const [Loading, setLoading] = useState(true)
   useEffect(() => {

      NetInfo.fetch().then(st => {
         if (st.isConnected != null) {
            st.isConnected ? setNet(true) : setNet(false)
            setLoading(false)
         }

      })
         .catch(e => console.log(e))
      setauthenticated(us.token != "")

   }
      , [us.token])


   const authWithNet =
      authenticated ?
         (

            <Stack.Navigator>
               <Stack.Screen
                  name="Landing"
                  component={TabsNavigation}
                  options={defaultNavigationSeetings}

               />
            </Stack.Navigator>
         )
         :
         (

      <Stack.Navigator>
         <Stack.Screen
            name="Home"
            component={Home}
            options={defaultNavigationSeetings}
         />
         <Stack.Screen
            name="SignIn"
            component={SignIn}
            options={defaultNavigationSeetings}
         /> 
         <Stack.Screen
            name="SignUp"
            component={SignUp}
            options={defaultNavigationSeetings}

         />
         <Stack.Screen
            name="Landing"
            component={TabsNavigation}
            options={defaultNavigationSeetings}

         />
      </Stack.Navigator>
   )

   const authWithoutNet =
      <Stack.Navigator>
         <Stack.Screen
            name={"Create"}
            component={CreateAccount}
         />
         <Stack.Screen
            name={"Login"}
            component={LoginAccount}
         />
      </Stack.Navigator>




   return (
      <View style={{
         flex: 1
      }}>
         {
            Loading ?
               (<LoadingScreen />)
               :
               (<Provider store={us}><NavigationContainer>
                  {
                     Net ?
                        (authWithNet)
                        :
                        (authWithoutNet)
                  }
               </NavigationContainer>
               </Provider>
               )
         }
      </View>
   );
   // return (
   //    <View style={{
   //       flex: 1,
   //       // backgroundColor: colors.background
   //    }}>
   //       <NavigationContainer >
   //          <TabsNavigation />
   //       </NavigationContainer>
   //    </View>

   // );
};

export default App;
